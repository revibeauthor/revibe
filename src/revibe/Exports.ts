// Copyright © 2016 Yevgeny Kurliandchick. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use
// this file except in compliance with the License. You may obtain a copy of the
// License at http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.

/// <reference path="../../thirdparty/typings/typescript/lib/lib.es6.d.ts"/>
/// <reference path="../../thirdparty/typings/node/node-4.d.ts"/>
module Revibe {
    export function Revibe
    (
        template: Object,
        options : RevibeOptions = {
            ErrorPolicy:"Abort",
            EnableRecursiveRevibe:false,
            EnableTrace:false
        }
    )
        : Promise<Object>
    {
        return new Reviber(options).Revibe(template);
    }

    export const VERSION = "0.0.1";
}

declare var exports;
exports.Revibe = Revibe.Revibe;
exports.version = Revibe.VERSION;
