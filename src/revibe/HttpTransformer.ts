// Copyright © 2016 Yevgeny Kurliandchick. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use
// this file except in compliance with the License. You may obtain a copy of the
// License at http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.

module Revibe
{
    const http = require('http');
    const https = require('https');
    const url = require('url');

    export interface HttpTransformerParams
    {
        Https : boolean
        Headers : {[key:string]: string};
        Method : string;
    }

    export class HttpTransformer extends TransformerBase<HttpTransformerParams> implements ExpressionTransformer
    {
        public constructor(expression: string, p:HttpTransformerParams) {
            super("http",expression,p);
        }

        public  GetDefaultParams() : HttpTransformerParams
        {
            return {
                Https:false,
                Headers:
                {
                    "User-Agent":"revibe " + VERSION
                },
                Method: "GET",
            };
        }

        public Transform(): Promise<ExpressionTransformResult>
        {
            return new Promise<ExpressionTransformResult>
            (
                (resolve,reject) =>
                {
                    try
                    {
                        const uri = url.parse(this.Params.Https?"https://":"http://"+this.Expression);
                        const httthing = this.Params.Https ? https : http;
                        const request = httthing.request(
                            {
                                protocol: uri.protocol,
                                hostname: uri.hostname,
                                port: uri.port,
                                method: this.Params.Method,
                                headers: this.Params.Headers,
                                path: uri.path
                            },
                            (response : any)  =>
                            {
                                let data = new Buffer("");

                                response.on('data',(chunk:Buffer)=>
                                {
                                    data = Buffer.concat([data,chunk]);
                                });
                                response.on('end',()=>
                                {
                                    resolve({value:data.toString()});
                                });
                                response.on('error',(err:Error)=>
                                {
                                    reject(err);
                                });
                            }
                        )
                        .on('error',(err:Error)=>
                        {
                            reject(err);
                        });
                        request.end();
                    }
                    catch (err)
                    {
                        reject(err);
                    }
                }
            );
        }
    }

}
