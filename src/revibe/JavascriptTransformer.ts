// Copyright © 2016 Yevgeny Kurliandchick. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use
// this file except in compliance with the License. You may obtain a copy of the
// License at http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.

module Revibe {

    export class JavascriptEvalTransformer extends TransformerBase<undefined> implements ExpressionTransformer
    {
        public constructor(expression: string) {
            super("js",expression,undefined);
        }

        public  GetDefaultParams() : undefined
        {
            return undefined;
        }

        public Transform(): Promise<ExpressionTransformResult>
        {
            return new Promise<ExpressionTransformResult>
            (
                (resolve,reject) =>
                {
                    try
                    {
                        resolve({
                            value:  JSON.parse(
                                        JSON.stringify( // detect problems with unserializable values early
                                            eval(this.Expression)
                                        )
                                    )
                        });
                    }
                    catch (err) {
                        reject(err);
                    }
                }
            );
        }
    }
}
