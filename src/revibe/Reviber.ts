// Copyright © 2016 Yevgeny Kurliandchick. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use
// this file except in compliance with the License. You may obtain a copy of the
// License at http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.

﻿module Revibe
{
    const os = require('os');
    export enum RevibeResultTypes
    {
        Comment,
        Literal,
        LiteralArray,
        JSON,
        Revibe
    }

    export class RevibeKey
    {
        constructor(resultPropName :string, resultType :RevibeResultTypes) {
            this._resultPropertyName = resultPropName;
            this._resultType = resultType;
        }

        public get ResultPropertyName(): string { return this._resultPropertyName; }
        private _resultPropertyName: string;

        public get ResultType(): RevibeResultTypes { return this._resultType; }
        private _resultType : RevibeResultTypes;

        public static get RevibeKeyRegex() {return new RegExp('(.*!)(.*)') };

        private static ResultTypeFromKeyPrefix(keyPrefix:string) : RevibeResultTypes
        {
            switch (keyPrefix)
            {
                case "//!": return RevibeResultTypes.Comment;
                case "!": return RevibeResultTypes.Literal;
                case "*!": return RevibeResultTypes.LiteralArray;
                case "!!": return RevibeResultTypes.JSON;
                case "!#!": return RevibeResultTypes.Revibe;
            }
            throw Error(`Unsupported key prefix ${keyPrefix}`);
        }

        public static FromJsonPropertyName(propName :string) : RevibeKey|null
        {
            const matches = RevibeKey.RevibeKeyRegex.exec(propName);

            if (!matches || matches.length != 3)
                return null;
            return new RevibeKey(
                matches[2],
                RevibeKey.ResultTypeFromKeyPrefix(matches[1]));
        }
    }

    export type ErrorPolicy = "Abort"|"Replace"|"Ignore";

    export interface RevibeOptions
    {
        ErrorPolicy : ErrorPolicy;
        EnableRecursiveRevibe : boolean;
        EnableTrace :boolean;
    }

    export class Reviber
    {
        constructor(options :RevibeOptions) {
            this._transformerManager = new TransformerManager();
            this._options = options;
        }
        private _transformerManager : TransformerManager;

        private _options : RevibeOptions;

        public Revibe(template: any): Promise<any>
        {
            if (Array.isArray(template))
                return this.RevibeArray(template);

            if (typeof template === 'object')
                return this.RevibeObject(template);

            return new Promise<any>((resolve,reject)=>resolve(template));
        }

        private RevibeObject(template:{}) : Promise<{}>
        {
            const target = {};
            const subPromises =
                Object
                .getOwnPropertyNames(template)
                .map
                (
                    propName =>
                    {
                        const revibeKey = RevibeKey.FromJsonPropertyName(propName);
                        if (revibeKey)
                        {
                            if (this._options.EnableTrace)
                                console.trace(`Transforming property ${propName} into ${revibeKey.ResultPropertyName||"<none>"} as ${RevibeResultTypes[revibeKey.ResultType]}`);
                            return this
                                .TransformProperty(template[propName], revibeKey.ResultType)
                                .then(r => this.HandleTransformResult(target, revibeKey.ResultPropertyName, r))
                        }
                        else
                        {
                            if (this._options.EnableTrace)
                                console.trace(`Transforming property ${propName}`);
                            return this
                                .Revibe(template[propName])
                                .then(r => this.HandleTransformResult(target, propName, {value: r}))
                        }
                    }
                );
            return Promise.all(subPromises).then(_ => target);
        }

        private RevibeArray(template:any[], membersAsExpressions :boolean=false) : Promise<any[]>
        {
            const target :any[] = [];
            const subPromises = template.map
            (
                (item,index)=>
                {
                    return membersAsExpressions
                        ?
                            this
                            .TransformProperty(item,RevibeResultTypes.JSON)
                            .then(r =>
                            {
                                this.HandleTransformResult(target,index,{value:r})
                            })
                        :
                            this
                            .Revibe(item)
                            .then(r =>
                            {
                                this.HandleTransformResult(target,index,{value:r})
                            })
                }
            );
            return Promise.all(subPromises).then( _ => target);
        }

        private HandleTransformResult(target:{},key : string | number, result:ExpressionTransformResult)
        {
            if (!result.error)
            {
                if (result.value) // result.value will be empty when tranforming a comment
                {
                    if (this._options.EnableTrace)
                        console.trace(`Assigning ${result.value} into ${key}`);
                    target[key] = result.value;
                }
                return;
            }
            // else
            switch(this._options.ErrorPolicy.toLowerCase())
            {
                case "abort":
                    throw(result.error);
                case "replace":
                    target[key]=result.error.toString();
                    break;
                case "ignore":
                    break;
            }
        }

        private TransformProperty(propValue: any,resultType : RevibeResultTypes): Promise<ExpressionTransformResult>
        {
            try
            {
                if (resultType == RevibeResultTypes.Comment)
                    return Promise.resolve({});

                const transformer  =
                (typeof propValue === 'object')
                    ?
                        this._transformerManager.GetTransformerForObjectNotation(propValue)
                    :
                        this._transformerManager.GetTransofrmerForShorthandNotation(propValue);

                if (!transformer)
                    return new Promise<any>((resolve,reject)=>reject(Error(`Could not get a transformer from ${propValue}`)));
      

                return transformer
                .Transform()
                .then(
                    v =>
                    {
                        switch (resultType)
                        {
                            case RevibeResultTypes.Literal:
                                return v;
                            case RevibeResultTypes.LiteralArray:
                                return {value: String(v.value).split(os.EOL) };
                            case RevibeResultTypes.JSON:
                                return {value:JSON.parse(v.value as string)};
                            case RevibeResultTypes.Revibe:
                                if (!this._options.EnableRecursiveRevibe)
                                    throw new Error('Recursive revibe not enabled');
                                else
                                    return (new Reviber(this._options)).Revibe(propValue)
                        }
                    }
                )
                .catch(error =>
                {
                    return {error:error};
                })
            }
            catch (err)
            {
                return new Promise<any>((resolve,reject)=>reject(err));
            }
        }

    }
}
