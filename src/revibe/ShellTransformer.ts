// Copyright © 2016 Yevgeny Kurliandchick. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use
// this file except in compliance with the License. You may obtain a copy of the
// License at http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.

module Revibe {

    export interface ShellTransformerParams
    {
        TrimTrailingNewline:boolean;
    }

    export class ShellCommandTransformer extends TransformerBase<ShellTransformerParams> implements ExpressionTransformer
    {
        public constructor(expression: string,p :ShellTransformerParams) {
            super("shell",expression,p);
            this.child_process = require('child_process');
        }
        private child_process : any;

        public  GetDefaultParams() : ShellTransformerParams
        {
            return {TrimTrailingNewline:true};
        }

        public Transform(): Promise<ExpressionTransformResult>
        {
            return new Promise<ExpressionTransformResult>
            (
                (resolve,reject) =>
                {
                    try
                    {
                        this.child_process.exec(
                            this.Expression,
                            null,
                            (error:Error,stdout:string,stderr:string) =>
                            {
                                if (error)
                                    reject(stderr);
                                else
                                    resolve(
                                    {
                                        value: this.Params.TrimTrailingNewline
                                            ?
                                                (stdout as string).trim()
                                            :
                                                (stdout as string)

                                    });
                            }
                        );
                    }
                    catch (err) {
                        reject(err);
                    }
                }
            );
        }
    }
}
