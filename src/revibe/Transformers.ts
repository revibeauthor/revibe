// Copyright © 2016 Yevgeny Kurliandchick. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use
// this file except in compliance with the License. You may obtain a copy of the
// License at http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.

﻿module Revibe {

    export interface ExpressionTransformResult
    {
        value?: any;
        error?: Error;
    }

    export interface ExpressionTransformer
    {
        Name: string;
        Transform: () => Promise<ExpressionTransformResult>;
    }

    export abstract class TransformerBase<TParams extends {} | undefined>
    {
        readonly Name :string;

        readonly Expression :string;

        readonly Params :TParams;

        public constructor(name :string, expression: string, params:TParams)
        {
            this.Expression = expression;
            this.Name = name;
            this.Params = Object.assign(params || {},this.GetDefaultParams());
        }

        public abstract GetDefaultParams() : TParams
    }

    export interface TransfromerObjectNotation
    {
        Type :string;
        Expression :string;
    }

    export class TransformerManager
    {
        public GetTransofrmerForShorthandNotation(shorthandExpression: string) : ExpressionTransformer
        {
            const iShorthandPrefix = shorthandExpression.indexOf('://');
            if (!iShorthandPrefix)
                throw Error("Invalid expression: '"+shorthandExpression+"'");

            const typename = shorthandExpression.substr(0,iShorthandPrefix);
            const expression = shorthandExpression.substr(iShorthandPrefix+3).trim();
            const retval = this.GetTransformer(typename,expression);
            if (!retval)
                throw Error("No transformer found for "+expression);
            return retval;
        }

        public GetTransformerForObjectNotation(objectNotation :TransfromerObjectNotation) : ExpressionTransformer|null
        {
            return this.GetTransformer(objectNotation.Type,objectNotation.Expression,objectNotation);
        }

        private GetTransformer(type :string,expression:string,transformerParams? :{}) : ExpressionTransformer|null
        {
            const factory = this.TransformerFactories[type];
            if (factory)
                return factory(expression,transformerParams);
            return null;
        }

        private TransformerFactories: { [prefix: string]: (expression :string,transformerParams?:{}) => ExpressionTransformer } =
        {
            "js": (expression,params) => { return new JavascriptEvalTransformer(expression) },
            "shell": (expression,params) => { return new ShellCommandTransformer(expression,params as ShellTransformerParams) },
            "file": (expression,params) => { return new FileTransformer(expression,params  as FileTransformerParams) },
            "http": (expression,params) => { return new HttpTransformer(expression, Object.assign(params || {},{Https:false})  as HttpTransformerParams) },
            "https": (expression,params) => { return new HttpTransformer(expression, Object.assign(params || {},{Https:true})  as HttpTransformerParams) },
        }
    }
}
