// Copyright © 2016 Yevgeny Kurliandchick. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use
// this file except in compliance with the License. You may obtain a copy of the
// License at http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.

module Rvb
{
    const os = require('os');
    const process = require('process');

    export interface CmdSwitch
    {
        LongForm :string;
        ShortForm :string;
        HelpText :string;
        ArgName :string|null;
        CmdOptional : boolean;
        ArgOptional : boolean;
        ArgValues? : string[];
    }

    export const errorPolicyValues = [
        "abort",
        "replace",
        "ignore"
    ];

    export const knownArgs :CmdSwitch[] =
    [
        {
            LongForm:"input",
            ShortForm:"i",
            ArgName:"file",
            HelpText:"revibe template file, or stdin without argument",
            CmdOptional:false,
            ArgOptional:true
        },
        {
            LongForm:"output",
            ShortForm:"o",
            ArgName:"file",
            HelpText:"json output file, or stdout without argument",
            CmdOptional:false,
            ArgOptional:true
        },
        {
            LongForm:"recursive",
            ShortForm:"r",
            ArgName:null,
            HelpText:"enable recursive revibe",
            CmdOptional:true,
            ArgOptional:true
        },
        {
            LongForm:"errorPolicy",
            ShortForm:"e",
            ArgName:"policy",
            HelpText:'transformation error behavior' ,
            CmdOptional:true,
            ArgOptional:false,
            ArgValues:errorPolicyValues
        },
        {
            LongForm:"version",
            ShortForm:"v",
            ArgName:null,
            HelpText:'print the current version' ,
            CmdOptional:true,
            ArgOptional:true
        },
        {
            LongForm:"trace",
            ShortForm:"t",
            ArgName:null,
            HelpText:'trace the transform for debugging' ,
            CmdOptional:true,
            ArgOptional:true
        }
    ];

    export function ParseArgs(argv :string[]) : RvbArgs|null
    {
        if (argv.length <= 2)
            return null;
        const retval :RvbArgs = {};

        const args = argv
            .slice(2)
            .reduce((a,b)=>a+' '+b)
            .split(/^-|\s+-/)
            .filter(s=>s.length>0)
            .map(s=>s.trim());

        args.forEach(arg=>
        {
            const parts = arg.split(RegExp(' +')).map(s=>s.trim());
            const printAllowedValues = (sw :CmdSwitch) =>
            {
                if (sw.ArgValues && sw.ArgValues.length)
                    console.error
                    (
                        "Allowed values for -"
                        +parts[0]
                        +" : "
                        +sw.ArgValues.reduce((a,b)=>a+','+b)
                    );
            }
            const sw = knownArgs.find
            (
                a=> a.LongForm == parts[0] || a.ShortForm == parts[0]
            );

            if (!sw)
            {
                console.error("Unsupported switch: ",parts[0]);
                process.exit(1);
            }

            if (parts.length > 1)
            {
                if (sw.ArgValues &&
                    !sw.ArgValues.find(w=>w.toLowerCase() == parts[1].toLowerCase()))
                {
                    printAllowedValues(sw);
                    process.exit(1);
                }
                else
                    retval[sw.LongForm] = parts[1];
            }
            else
            {
                if (!sw.ArgOptional)
                {
                    console.error("-"+parts[0]+" requires an argument");
                    printAllowedValues(sw);
                    process.exit(1);
                }
                retval[sw.LongForm] = true;
            }

        });
        //console.log(retval);
        return retval;
    }

    export function PrintUsage()
    {
        const compareByOptionality = (a:CmdSwitch,b:CmdSwitch) =>
            a.CmdOptional
                ? b.CmdOptional
                    ? a.ShortForm[0] < b.ShortForm[0]
                        ? 1 : -1
                    : 1
                : -1
        // for academy award consideration

        const renderInline =  (o : CmdSwitch) =>
            (o.CmdOptional?'[':'')
            +'-'
            +o.ShortForm
            +
            (
                o.ArgName
                    ?
                    (
                        (o.ArgOptional?' [<':' <')
                        +o.ArgName+'>'
                        +(o.ArgOptional?']':'')
                    )
                    :''
            )
            +(o.CmdOptional?'] ':' ')

        const renderHelp = (o : CmdSwitch) =>
            '-'
            +o.ShortForm
            +"|"
            +o.LongForm
            +(
                o.ArgName
                ?
                    (
                        ' '
                        +(o.ArgOptional?'[':'')
                        +'<'
                        +o.ArgName
                        +'>'
                        +(o.ArgOptional?']':'')
                    )
                :
                ""
            )
            +" - "
            +o.HelpText
            +(

                o.ArgValues
                ?
                    ": "+o.ArgValues.reduce((a,b)=>a+","+b)
                :
                    ""
            );

        console.log(os.EOL);
        console.log('rvb - JSON preprocessor')

        console.log(
                'Usage: rvb '+knownArgs
            .sort(compareByOptionality)
            .map(o => renderInline(o))
            .reduce((a,b)=>a+' '+b));
        console.log(os.EOL);
        console.log(
            knownArgs
            .sort(compareByOptionality)
            .map(o => renderHelp(o))
            .reduce((a,b)=>a+os.EOL+b));
        console.log(os.EOL);
    }

}
