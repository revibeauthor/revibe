// Copyright © 2016 Yevgeny Kurliandchick. All rights reserved.
//
// Licensed under the Apache License, Version 2.0 (the "License"); you may not use
// this file except in compliance with the License. You may obtain a copy of the
// License at http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED ON AN *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
//
// See the Apache Version 2.0 License for specific language governing permissions
// and limitations under the License.

/// <reference path="../../artifacts/revibe/revibe.d.ts"/>
module Rvb
{
    const revibe = require('./revibe');
    const fs = require('fs');
    const os = require('os');
    const process = require('process');

    const version = "revibe" + revibe.Revibe.VERSION;

    export interface RvbArgs
    {
        input? :string;
        output? :string;
        recursive? :boolean;
        trace? :boolean;
        version? :boolean;
        errorPolicy?: Revibe.ErrorPolicy;
    }

    function HandleError(err :Error)
    {
        console.error(err);
        process.exit(-1);
    }

    function Run(args :RvbArgs, template:{})
    {
        revibe
        .Revibe
        (
            template,
            {
                ErrorPolicy:args.errorPolicy?args.errorPolicy:"abort",
                EnableRecursiveRevibe: args.recursive?true:false,
                EnableTrace:args.trace?true:false
            }
        )
        .then(r=>
        {
            const outputStream = args.output && args.output.length
                ? fs.createWriteStream(args.output,"utf8",err=>HandleError(err))
                : process.stdout;
            outputStream.on('error',HandleError);
            outputStream.write(JSON.stringify(r,(k,v)=>v,4)+os.EOL);
        })
        .catch(err=>HandleError(err))
    }

    const args = ParseArgs(process.argv);
    if (!args)
    {
        PrintUsage();
        process.exit(1);
        throw Error(''); // to enable typescript type guard on args not-null
    }

    if (args.version)
    {
        console.log(version);
        process.exit(0);
    }

    const inputStream = args.input && args.input.length
        ?fs.createReadStream(args.input,'utf8',err=>HandleError(err))
        :process.stdin;

    let templateJson :string="";
    inputStream.setEncoding('utf8');
    inputStream.on('error',err=>HandleError(err));
    inputStream.on('data',data=>
    {
        templateJson += data;
    });
    inputStream.on('end',()=>
    {
        if (templateJson.length==0)
            process.exit(0);
        Run(args, JSON.parse(templateJson));
    });
    inputStream.resume();
}
