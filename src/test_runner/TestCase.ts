module Revibe.Tests
{
    const fs = require('fs');
    const os = require('os');
    const process = require('process');
    const child_process = require('child_process');
    const path = require('path');
    const dd = require('../thirdparty/deep-diff/deep-diff-0.3.4.min.js');

    export enum TestResultStatus
    {
        // Test has passed
        Passed,
        // Test failed because the resulting output was different from expected
        Failed,
        // Test failed because an error occured during it's execution
        Exception
    }

    export class TestResult
    {
        public constructor(
            name :string,
            status :TestResultStatus
            )
        {
            this._name = name;
            this._status = status;
        }

        public get Status() : TestResultStatus {return this._status;}
        private _status : TestResultStatus;

        public get Name() : string {return this._name;}
        private _name : string;
    }

    export class TestCase
    {
        public constructor(rvbExec:string, dir: string, enc="utf8" )
        {
            this.name = dir;
            this.dir = fs.realpathSync(dir);
            this.outDir = path.join(dir,"out");
            this.enc = enc;
            this.rvbExec = rvbExec;
        }

        name:string;
        dir: string;
        outDir:string;
        enc: string;
        rvbExec:string;

        IN_FILENAME: string = "in.rvb";
        OPTIONS_FILENAME: string = "options"
        ACTUAL_OUT_FILENAME: string = "actual.out";
        ACTUAL_ERR_FILENAME: string = "actual.err";
        EXPECTED_OUT_FILENAME: string = "expected.out";
        EXPECTED_ERR_FILENAME: string = "expected.err";
        JSON_DIFF_FILENAME: string = "diff.json";
        ALLOWED_DIFFS_FILENAME: string = "allowed_diffs.json";

        private Clean() :void
        {
            if (!fs.existsSync(this.outDir))
                return;

            fs
            .readdirSync(this.outDir)
            .forEach(filename=>fs.unlinkSync(path.join(this.outDir,filename)));

            fs.rmdirSync(this.outDir);
        }

        private Verify(resultJson:string,expectedJson:string) :  TestResultStatus
        {
            const diffs = dd.diff(
                JSON.parse(resultJson),
                JSON.parse(expectedJson)
            );

            if (!diffs)
                return TestResultStatus.Passed;

            const errors =
                (diffs as any[])
                .filter(diff=>
                {
                    return !diff["rhs"] || diff["rhs"]!="$ignore$";
                });

            if (!errors.length)
                return TestResultStatus.Passed;

            fs.writeFileSync(
                path.join(this.outDir,this.JSON_DIFF_FILENAME),
                JSON.stringify(errors, (k,v)=>v, 4),
                { ecnoding: this.enc }
            );
            return TestResultStatus.Failed;
        }

        private SetupServer()
        {
            if (!fs.existsSync('setup.js'))
                return;
            //const child = child_process.exec('node ./setup.js');
        }


        public Run() : TestResult
        {
            this.Clean();

            if (!fs.existsSync(this.outDir))
                fs.mkdirSync(this.outDir);

            process.chdir(this.dir);
            const options = fs.existsSync(this.OPTIONS_FILENAME)
                    ? fs.readFileSync(this.OPTIONS_FILENAME)
                    : '';

            const outFile = path.join(this.outDir,this.ACTUAL_OUT_FILENAME);
            const cmd = this.rvbExec +' -i '+this.IN_FILENAME+' -o '+ outFile +' ' + options;
            child_process.execSync(cmd);
            const result = fs.readFileSync(outFile);
            const expected = fs.readFileSync(this.EXPECTED_OUT_FILENAME);
            const status = this.Verify(result,expected);
            return new TestResult(this.name,status);
        }
    }
}
