/// <reference path="../../thirdparty/typings/typescript/lib/lib.es6.d.ts"/>
/// <reference path="../../thirdparty/typings/node/node-4.d.ts"/>
module Revibe.Tests
{
    const fs = require('fs');
    const process = require('process');
    const path = require('path');

    if (process.argv.length < 4)
    {
        console.log(`Usage: ${process.argv[0]} ${process.argv[1]} <path to rvb> <tescases dir>`);
        process.exit();
    }

    const rvbExec = fs.realpathSync(process.argv[2]);
    const base = fs.realpathSync(process.argv[3]);
    process.chdir(base);

    fs
    .readdirSync(base)
    .filter(f=>fs.lstatSync(fs.realpathSync(f)).isDirectory())
    .forEach(d=>
    {
        const testDir = path.join(base,d);
        console.log("Running: "+testDir);
        const testResult = (new TestCase(rvbExec,testDir)).Run()
        if (testResult.Status != TestResultStatus.Passed)
        {
            console.error("Failed: ",testResult.Name);
            process.exit(1);
        }
        else
        {
            console.log("Passed: ",testResult.Name);
        }
    })
}
