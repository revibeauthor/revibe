A functional test suite - a corpus of input templates and the expected results of their transformations

### The structure of a test case folder

##### Mandatory
* `in.rvb` - the test case revibe template
* `expected.out` - expected output to be compared both as json object
and a textfile (test formating options)

##### Optional
* `options` - optional command line arguments for rvb
* `expected.err` - expected content of stderr.
* `assets/` arbitrary files used by the test template - convetion, include the directory when referring in the test template
* TBD `server.js` - js file to be run in a child process to setup an optional server for testing
* TBD `allowed_diffs.json` - allowed JSON diffs, use `"$ignore$"` as value for now to indicate that diffs in the value of this field shouldn't fail the test

##### Created by the test runner
* `out/actual.out` - the transfromation result
* `out/actual.err` - the contents of stderr
* `out/diff.json` - a deep diff between the expected and the actual JSON objects
